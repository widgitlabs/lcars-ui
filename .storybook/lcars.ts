import { create } from "@storybook/theming/create";

export default create({
    base: "dark",
    brandTitle: "LCARS UI Storybook",
    brandUrl: "https://lcars-ui.com",
    brandImage:
        "https://federationhistory.com/wp-content/uploads/2024/02/lcars-ui-sb.png",
    brandTarget: "_self",
});
