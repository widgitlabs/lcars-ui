import { addons } from "@storybook/manager-api";
import lcarsTheme from "./lcars";

addons.setConfig({
    theme: lcarsTheme,
});
