# LCARS UI

[![License: MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/widgitlabs/guidebook/guidebook/blob/main/LICENSE.txt)
[![Pipelines](https://gitlab.com/widgitlabs/guidebook/guidebook/badges/master/pipeline.svg)](https://gitlab.com/widgitlabs/guidebook/guidebook/pipelines)
[![Discord](https://img.shields.io/discord/586467473503813633?color=899AF9)](https://discord.gg/jrydFBP)

![LCARS UI](https://federationhistory.com/wp-content/uploads/2024/02/lcars-ui-hero.png)

LCARS UI is a set of open source UI components for React based on the LCARS
interface designed by Michael Okuda.

## Quick Start

LCARS UI is available as an [NPM package](https://www.npmjs.com/package/@widgitlabs/lcars-ui).

### Installation

```sh
# With yarn
yarn add @widgitlabs/lcars-ui

# With NPM
npm install @widgitlabs/lcars-ui

# With pnpm
pnpm add @widgitlabs/lcars-ui
```
